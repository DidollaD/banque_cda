package fr.afpa.control;



/**
 * Methode qui permets de valider le numero de la rue
 * @param num : prends en parametre le numero de la rue 
 *
 */
public class AgenceControl {
	public boolean validerNumero(String num) {
		if (num != null && num.matches("\\d+")) return true;
		return false;
	}
	
	/**
	 * Methode qui permets de valider la bonne ecriture de la rue
	 * @param rue
	 * @return
	 */
	public boolean validerRue(String rue) {
		if (rue != null && rue.matches("[0-9 a-zA-Z]+")) return true;
		return false;
	}
	
	/**
	 * Methode qui permets de valider le code postal
	 * @param codePostal : prends en parametre le code postal
	 * @return
	 */
	public boolean validerCodePostal(String codePostal) {
		if (codePostal != null && codePostal.matches("\\d{5}")) return true;
		return false;
	}
	
	/**
	 * Methode qui permets de valider la ville
	 * @param ville prends en parametre la ville 
	 * @return
	 */
	public boolean validerVille(String ville) {
		if (ville != null && ville.matches("[a-zA-Z]+")) return true;
		return false;
	}
}
