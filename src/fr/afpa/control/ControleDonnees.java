package fr.afpa.control;

import java.time.LocalDate;

public class ControleDonnees {

	/**
	 * Methode qui permets de valider le mot de passe du client
	 * @param mdpClient : prends en parametre le mot de passe
	 * @return
	 */
	public static boolean validerMotDePasseClient(String mdpClient) {
		if (mdpClient != null && mdpClient.matches("[a-zA-Z\\d]{7,}"))
			return true;

		return false;
	}

	/**
	 * Methode qui permets de valider le nom et prenom du client
	 * @param nomClient: prends en parametre le nom ou prenom du client entree
	 * @return
	 */
	public static boolean validerNomPrenomClient(String nomClient) {
		if (nomClient != null && nomClient.matches("[a-zA-Z]{2,}"))
			return true;

		return false;
	}

	/**
	 * Methode qui retourne true si la chaine de caractere est un mail valide
	 */
	public static boolean validerAdresseMailClient(String emailClient) {
		if (emailClient != null && emailClient.matches("\\w[\\w\\-]*(\\.[\\w\\-]+)*@[a-zA-Z]{3,13}\\.[a-zA-Z]{2,3}"))
			return true;

		return false;
	}
	/**
	 * Methode qui permets de valider la date 
	 * @param date : prends en parametre la date entree
	 * @return
	 */

	public static boolean ValiderDate(LocalDate date) {
		if (date.isAfter(LocalDate.now()))
			return false;
		return true;
	}
	/**
	 * Methode qui permets de valider la date de fin a l'impression 
	 * @param date : prends en parametre la dta entre
	 * @param datefin : prends en parametre la date de fin 
	 * @return
	 */
	public static boolean ValiderDateFin(LocalDate date,LocalDate datefin) {
		if (date.isAfter(date))return false;
		return true;
	}
}
