package fr.afpa.control;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;
import fr.afpa.services.UtilService;

public class UtilControl {
	public static Personne ControlLogin(ArrayList<Agence> agences, Admin admin, String login, Scanner sc) {
		UtilService us = new UtilService();
		String mdp = null;
		if (login.length() == 5) {
			if (login.equals(admin.getId())) {

				System.out.println("Saisir votre mot de passe");
				mdp = sc.next();
				sc.nextLine();
				if (mdp != null && mdp.equals(RecuperationPassAdmin())) {
					return admin;
				}
			}
		} else if (login.length() == 6) {
			Conseiller conseiller = us.RechercheConseiller(agences, login);
			if (conseiller != null) {
				System.out.println("Saisir votre mot de passe");
				mdp = sc.next();
				sc.nextLine();
				if (mdp.equals(conseiller.getMdp())) {
					return conseiller;
				}
			}
		} else if (login.length() == 11) {
			Client client = us.RechercheClientLogin(agences, login);
			if (client != null) {
				System.out.println("Saisir votre mot de passe");
				mdp = sc.next();
				sc.nextLine();
				if (mdp.equals(client.getMdp())) {
					return client;
				}
			}
		}
		System.out.println("Saisie invalide");
		return null;

	}

	private static String RecuperationPassAdmin() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(".\\save\\clasifie.txt"));
			String str = br.readLine();
			br.close();
			str = Cesar(str);
			return str;
		} catch (Exception e) {
			System.out.println("non");
		}
		return null;
	}

	private static String Cesar(String str) {
		String str2 ="";
		for (int i = 0; i < str.length(); i++) {
			if ((str.charAt(i) >= 51 && str.charAt(i) <= 57)||(str.charAt(i) >= 68 && str.charAt(i) <= 90)||(str.charAt(i) >= 100 && str.charAt(i) <= 122)) {
				str2 += (char)(str.charAt(i)-3);
			}
			if ((str.charAt(i) >=65  && str.charAt(i) <= 67)) {
				str2 += (char)(str.charAt(i)-10);
			}
			if ((str.charAt(i) >= 97 && str.charAt(i) <= 100)) {
				str2 += (char)(str.charAt(i)-9);
			}
			if ((str.charAt(i) >=48  && str.charAt(i) <= 50)) {
				str2 += (char)(122-(50-str.charAt(i)));
			}
		}
		return str2;
	}

	public static boolean verifierMontant(double montant) {
		if (montant > 0)
			return true;
		return false;
	}
}
