package fr.afpa.control;

import fr.afpa.entite.Compte;

public class ControleSoldeCompte {

	/**
	 * M�thode qui permets de confirmer le solde d'un compte
	 * @param montant : le montant 
	 * @param compte : le compte a verifier
	 * @return
	 */
	
	public static boolean validerSoldeCompte(double montant, Compte compte) {

		if (compte.getSolde() < montant && compte.isDecouvertAutorise() == false) {
			System.out.println("d�couvert non autoris�");
			return false;
		}
		return true;
	}
}
