package fr.afpa.entite;

import java.util.ArrayList;

public class Conseiller extends Personne{
	
	private static int derIdConseiller;
	private ArrayList<Client> clients;

	// Constructeur //
	public Conseiller(String nomConseiller, String prenomConseiller, String mdpConseiller) {
		super();
		this.nom = nomConseiller;
		this.prenom = prenomConseiller;
		this.id = String.format("CO%04d", ++derIdConseiller);
		this.mdp = mdpConseiller;
		clients = new ArrayList<Client>();
	}

	public static int getDerIdConseiller() {
		return derIdConseiller;
	}

	public ArrayList<Client> getClients() {
		return clients;
	}

	public void setClients(ArrayList<Client> clients) {
		this.clients = clients;
	}

	@Override
	public String toString() {
		return "Conseiller " + id + ", mdp : " + mdp;
	}

}
