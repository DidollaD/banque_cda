package fr.afpa.entite;

public class LivretA extends CompteCourant {
	/**
	 * Methode pour calculer les montants en lien avec le livret A
	 */
	public LivretA() {
		super(false);
		frais = "25 euros + 10% de l��pargne / an";
	}

	@Override
	public double calculFrais() {

		return super.calculFrais()+(solde*0.1);
	}

	/**
	 * To String
	 */
	@Override
	public String toString() {
		return "LivretA          "+ numeroCompte + "         " + solde +"        "+decouvertAutorise+"    "+ frais ;
	}

}
