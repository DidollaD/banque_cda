package fr.afpa.entite;

public class PlanEpargneLogement extends CompteCourant {
	
	/**
	 * Methode pour calculer epargne logement
	 */
	public PlanEpargneLogement() {
		super(false);
		frais = "25 euros + 2.5% de l��pargne / an";
	}
/**
 * Methode calcul frais
 */
	@Override
	public double calculFrais() {
		return super.calculFrais()+(solde*0.025);
	}
	/**
	 * To String
	 */
	@Override
	public String toString() {
		return "PEL              "+ numeroCompte + "         " + solde +"       "+decouvertAutorise+"    "+ frais ;
	}
}
