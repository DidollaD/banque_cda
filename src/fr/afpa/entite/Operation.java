package fr.afpa.entite;

import java.time.LocalDate;

public class Operation {

	private String typeOperation;
	private Double somme;
	private LocalDate dateOperation;
	private String compteCredite;

	/**
	 * Le Constructeur
	 * 
	 * @param typeOperation
	 * @param somme
	 * @param dateOperation
	 */
	public Operation(String typeOperation, LocalDate dateOperation, double somme, String compteCredite) {
		super();
		this.typeOperation = typeOperation;
		this.somme = somme;
		this.dateOperation = dateOperation;
		this.compteCredite = compteCredite;
	}

	public Operation(String typeOperation, double montant) {
		super();
		this.typeOperation = typeOperation;
		this.somme = montant;
		this.dateOperation = LocalDate.now();
		this.compteCredite = "";
	}

	/**
	 * Getters & Setters
	 * 
	 * @return
	 */
	public String getTypeOperation() {
		return typeOperation;
	}

	public void setTypeOperation(String typeOperation) {
		this.typeOperation = typeOperation;
	}

	public Double getSomme() {
		return somme;
	}

	public void setSomme(Double somme) {
		this.somme = somme;
	}

	public LocalDate getDateOperation() {
		return dateOperation;
	}

	public void setDateOperation(LocalDate dateOperation) {
		this.dateOperation = dateOperation;
	}

	public String getCompteCredite() {
		return compteCredite;
	}

	public void setCompteCredite(String compteCredite) {
		this.compteCredite = compteCredite;
	}

	/**
	 * To String
	 */
	@Override
	public String toString() {
		return typeOperation + "     "+ somme + "     " + dateOperation;
	}

}
