package fr.afpa.entite;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Client extends Personne{
	private String loginClient;
	private String email;
	private LocalDate dateNaissance;
	private boolean active;
	private ArrayList<Compte> comptes;
	private static int derIdClient;
	
	public Client(String loginClient,String mdpClient, String nom, String prenom, String email,
			LocalDate dateNaissance) {
		super();
		this.loginClient = loginClient;
		this.mdp = mdpClient;
		this.id = String.format("AF%06d", ++derIdClient);
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.dateNaissance = dateNaissance;
		active = true;
		this.comptes = new ArrayList<Compte>();
	}

	@Override
	public String toString() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return "login : " + loginClient + " mot de passe : " + mdp + " identifiant client : " + id + " nom : "
				+ nom + " prenom : " + prenom + " addresse email : " + email + " date de naissance : " + dtf.format(dateNaissance) ;
	}

	public String toStringNl() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return "login : " + loginClient + "\nmot de passe : " + mdp + "\nidentifiant client : " + id + "\nnom : "
				+ nom + "\nprenom : " + prenom + "\naddresse email : " + email + "\ndate de naissance : " + dtf.format(dateNaissance) ;
	}
	// Getters & Setters //

	public String getLoginClient() {
		return loginClient;
	}

	public void setLoginClient(String loginClient) {
		this.loginClient = loginClient;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ArrayList<Compte> getComptes() {
		return comptes;
	}

	public void setComptes(ArrayList<Compte> comptes) {
		this.comptes = comptes;
	}
	
}
