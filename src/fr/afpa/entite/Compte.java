package fr.afpa.entite;

import java.util.ArrayList;

public class Compte {

	protected String numeroCompte;
	protected double solde;
	protected boolean decouvertAutorise;
	protected boolean active;
	private static int derNumeroCompte;
	ArrayList<Operation> operations;

	public Compte(Boolean decouvertAutorise) {
		super();
		this.numeroCompte = String.format("%010d", ++derNumeroCompte);
		this.solde = 0.0;
		this.decouvertAutorise = decouvertAutorise;
		active = true;
		this.operations = new ArrayList<Operation>();
	}

	/**
	 * Getters & Setters
	 * 
	 * @return
	 */

	public String getNumeroCompte() {
		return numeroCompte;
	}

	public Double getSolde() {
		return solde;
	}

	public void setSolde(Double solde) {
		this.solde = solde;
	}

	public Boolean isDecouvertAutorise() {
		return decouvertAutorise;
	}

	public void setDecouvertAutorise(Boolean decouvertAutorise) {
		this.decouvertAutorise = decouvertAutorise;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ArrayList<Operation> getOperations() {
		return operations;
	}

	public void setOperations(ArrayList<Operation> operations) {
		this.operations = operations;
	}

	/**
	 * ToString
	 */

	@Override
	public String toString() {
		return numeroCompte + "         " + solde +"        "+decouvertAutorise+"    " ;
	}
}
