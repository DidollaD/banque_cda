package fr.afpa.entite;

public class CompteCourant extends Compte {
	protected String frais;
	public CompteCourant(Boolean decouvertAutorise) {
		super(decouvertAutorise);
		frais = "25 euros / an";
	}
	
	// getter //
	public String getFrais() {
		return frais;
	}
	@Override
	public String toString() {
		return "compte courant   "+super.toString()+frais ;
	}
	
	/**
	 * Methode pour calculer les frais
	 * @return
	 */
	public double calculFrais() {
		return 25;
	}
}
