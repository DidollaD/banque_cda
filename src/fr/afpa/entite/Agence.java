package fr.afpa.entite;

import java.util.ArrayList;

public class Agence {
	private String codeAgence;
	private String numAddrAgence;
	private String rueAgence;
	private String codePostalAgence;
	private String villeAgence;
	private ArrayList<Conseiller> conseillers;
	private static int derCodeAgence;
	
	public Agence(String numAddrAgence,String rueAgence,String codePostalAgence,String villeAgence) {
		super();
		this.codeAgence = String.format("%03d", ++derCodeAgence);
		this.numAddrAgence = numAddrAgence;
		this.rueAgence =rueAgence;
		this.codePostalAgence = codePostalAgence;
		this.villeAgence = villeAgence;
		this.conseillers = new ArrayList<Conseiller>();
	}

	// Getters et Setters //
	
	public String getCodeAgence() {
		return codeAgence;
	}
		
	public String getNumAddrAgence() {
		return numAddrAgence;
	}
	

	public void setNumAddrAgence(String numAddrAgence) {
		this.numAddrAgence = numAddrAgence;
	}

	public String getRueAgence() {
		return rueAgence;
	}

	public void setRueAgence(String rueAgence) {
		this.rueAgence = rueAgence;
	}

	public String getCodePostalAgence() {
		return codePostalAgence;
	}

	public void setCodePostalAgence(String codePostalAgence) {
		this.codePostalAgence = codePostalAgence;
	}

	public String getVilleAgence() {
		return villeAgence;
	}

	public void setVilleAgence(String villeAgence) {
		this.villeAgence = villeAgence;
	}

	public ArrayList<Conseiller> getConseillers() {
		return conseillers;
	}

	public void setConseillers(ArrayList<Conseiller> conseillers) {
		this.conseillers = conseillers;
	}

	@Override
	public String toString() {
		return "Agence " + codeAgence + ", " + numAddrAgence + " " + rueAgence
				+ " " + codePostalAgence + " " + villeAgence ;
	}

	

	
}
