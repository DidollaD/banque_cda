package fr.afpa.ihm;

import java.util.ArrayList;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Operation;

public class Affichage {

	/**
	 * Methode pour afficher le menu du client
	 */

	public void menuClient() {
		System.out.print("\n\n" + "A - Peut consulter ses informations\n" + "B - Consulter ses comptes\n"
				+ "C - Consulter les op�rations de chaque compte\n" + "D - Faire un virement\n"
				+ "E - Imprimer un relev� de compte\n" + "F - Alimenter son compte\n" + "G - Retirer argent\n");
	}

	/**
	 * Menu pour afficher le menu du conseiller
	 */
	public void menuConseiller() {
		menuClient();
		System.out.print("H - Cr�er un compte\n" + "I - Cr�er un client\n"
				+ "J - Changer la domiciliation d�un client\n" + "K - Modifier les informations d�un client\n");
	}

	/**
	 * Menu pour afficher le menuAdmin
	 */

	public void menuAdmin() {
		menuConseiller();
		System.out.println("L - Cr�er une agence\n" + "M - Cr�er un conseiller\n" + "N - D�sactiver un compte\n"
				+ "O - D�sactiver un client\n" + "T - Afficher tout\n");
	}

	public void AfficherTout(ArrayList<Agence> agences) {
		for (Agence agence : agences) {
			System.out.println(agence.toString());
			for (Conseiller conseiller : agence.getConseillers()) {
				System.out.println("   " + conseiller.toString());
				for (Client client : conseiller.getClients()) {
					if (client.isActive())System.out.println("      " + client.toString());
					for (Compte compte : client.getComptes()) {
						if (compte.isActive())System.out.println("         " + compte.toString());
						for (Operation operation : compte.getOperations()) {
							System.out.println("            " + operation.toString());
						}
					}
				}
			}
		}
	}
}
