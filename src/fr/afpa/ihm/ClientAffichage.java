package fr.afpa.ihm;

import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Operation;

public class ClientAffichage {
	
	/**
	 * Methode qui permets de consulter les informations du client
	 * @param client
	 */
	public void ConsulterInformation(Client client) {
		System.out.println("Information : \n"+client.toStringNl());
	}
	
	/**
	 * Methode pour consulter le compte du client
	 * @param client : prends en parametre le compte du client
	 */
	public void ConsulterCompte(Client client) {
		System.out.println("Comptes : ");
		System.out.println("type de compte   Num�ro de compte   Solde     D�couvert      Frais");
		for (Compte compte : client.getComptes()) {
			if (compte.isActive()) System.out.println(compte.toString());
		}	
	}
	/**
	 * Methode pour consulter l'operation du compte
	 * @param client :  prends en parametre le compte du client
	 */
	public void ConsulterOperationCompte(Client client) {
		System.out.println("Operations : ");
		for (Compte compte : client.getComptes()) {
			if(compte.isActive()) {
				System.out.println("Comptes : " + compte.getNumeroCompte() + " : ");
				for (Operation operation : compte.getOperations()) {
					System.out.println(operation.toString());
				}
			}
			
		}	
	}
}
