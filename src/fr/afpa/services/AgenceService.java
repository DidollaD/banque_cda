package fr.afpa.services;

import java.time.LocalDate;
//import java.io.BufferedReader;
//import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.control.AgenceControl;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.LivretA;
import fr.afpa.services.ConseillerService;


public class AgenceService {
	public ArrayList<Agence> creerListAgence() {
		ArrayList<Agence> listAgences = new ArrayList<Agence>();
//		try {
//			BufferedReader br = new BufferedReader(new FileReader(".\\save\\ListeAgence.csv"));
//			br.readLine();
//			while (br.ready()) {
//				String[] str = br.readLine().split(";");
//				listAgences.add(new Agence(str[0],str[1],str[2],str[4],str[5]));
//				
//			}
//			br.close();
//		} catch (Exception e) {
//			System.out.println("Erreur liste Agence" + e);
//		}
		listAgences.add(new Agence("4", "rue du pindre de coninc", "59270", "besace"));
		listAgences.add(new Agence("8", "rue 30 fevrier", "33241", "bajou sous gorgerin"));
		listAgences.get(0).getConseillers().add(new Conseiller("Vincent", "Didier", "12345678"));
		listAgences.get(1).getConseillers().add(new Conseiller("Vincent", "Richard", "12345678"));
		listAgences.get(0).getConseillers().get(0).getClients().add(new Client("12345678901","12345678", "Alpha", "Alponce", "iddidoll@gmail.com", LocalDate.of(1995, 11, 28)));
		listAgences.get(0).getConseillers().get(0).getClients().add(new Client("12345678902","12345678", "Bravo", "Christine", "iddidoll@gmail.com", LocalDate.of(1968, 11, 11)));
		listAgences.get(1).getConseillers().get(0).getClients().add(new Client("12345678903","12345678", "Charlie", "Charle", "iddidoll@gmail.com", LocalDate.of(1987, 11, 04)));
		listAgences.get(0).getConseillers().get(0).getClients().get(0).getComptes().add(new CompteCourant(true));
		listAgences.get(0).getConseillers().get(0).getClients().get(0).getComptes().add(new LivretA());
		listAgences.get(1).getConseillers().get(0).getClients().get(0).getComptes().add(new CompteCourant(true));
		
		return listAgences;
	}
	
	/**
	 * Methode qui permets de creer une agence
	 * @param sc : scanner pour recuperer les reponses
	 * @return
	 */
	
	public Agence creerAgence(Scanner sc) {
		AgenceControl ac = new AgenceControl();
		String numAddr = null;
		String rue = null;
		String codePostal = null;
		String ville = null;
		
		do {
			System.out.println("Veuillez entrer le numero de la rue de l'agence : ");
			numAddr = sc.next();
			sc.nextLine();
			if (ac.validerNumero(numAddr) == false) {
				System.out.println("Saisie Invalide");
				numAddr = null;
			} 
		} while (numAddr == null);
		do {
			System.out.println("Veuillez entrer la rue de l'agence : ");
			rue = sc.nextLine();
			if (ac.validerRue(rue) == false) {
				System.out.println("Saisie Invalide");
				rue = null;
			} 
		} while (rue == null);
		do {
			System.out.println("Veuillez entrer le code postal de l'agence : ");
			codePostal = sc.next();
			sc.nextLine();
			if (ac.validerCodePostal(codePostal) == false) {
				System.out.println("Saisie Invalide");
				codePostal = null;
			} 
		} while (codePostal == null);
		do {
			System.out.println("Veuillez entrer la ville de l'agence : ");
			ville = sc.nextLine();
			if (ac.validerVille(ville) == false) {
				System.out.println("Saisie Invalide");
				ville = null;
			} 
		} while (ville == null);
		return new Agence(numAddr,rue,codePostal,ville);
	}
	
	/**
	 * Methode qui recherche un conseiller dans une agence
	 * @param agence : agence dans la quelle se trouve le conseiller
	 * @param idConseiller : conseiller
	 * @return
	 */
	
	public Conseiller RechercheConseiller(Agence agence, String idConseiller) {
		for (Conseiller conseiller : agence.getConseillers()) {
			if (idConseiller.equals(conseiller.getId()))
				return conseiller;
		}
		return null;
	}
	
	/**
	 * Methode pour rechercher un client dans une agence
	 * @param agence : agence du client
	 * @param idClient : client concerne
	 * @return
	 */

	public Client RechercheClient(Agence agence, String idClient) {
		ConseillerService cns = new ConseillerService();
		for (Conseiller conseiller : agence.getConseillers()) {
			return cns.RechercheClient(conseiller, idClient);
		}
		return null;
	}

	/**
	 * Methode pour rechercher le login d un client
	 * @param agence : agence du client
	 * @param loginClient : login du client
	 * @return
	 */
	public Client RechercheClientLogin(Agence agence, String loginClient) {
		ConseillerService cns = new ConseillerService();
		for (Conseiller conseiller : agence.getConseillers()) {
			return cns.RechercheClientLogin(conseiller, loginClient);
		}
		return null;
	}
	
	/**
	 * Methode pour rechercher un compte 
	 * @param agence : agence a laquelle appartient le compte
	 * @param numCompte : numero de copmte
	 * @return
	 */
	
	public Compte RechercheCompte(Agence agence, String numCompte) {
		ConseillerService cns = new ConseillerService();
		for (Conseiller conseiller : agence.getConseillers()) {
			return cns.RechercheCompte(conseiller, numCompte);
		}
		return null;
	}
	

}
