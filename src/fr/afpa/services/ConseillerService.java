package fr.afpa.services;

import java.util.Scanner;

import fr.afpa.control.ControleDonnees;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;

public class ConseillerService {

	/**
	 * Methode qui permets de creer le nouveau conseiller
	 * @param sc : recuperer la reponse
	 * @return
	 */
	public Conseiller creerNouveauConseiller(Scanner sc) {
		String nom = null;
		String prenom = null;
		String mdp = null;

		do {
			System.out.println("Veuillez entrer le mdp du conseiller : ");
			mdp = sc.next();
			sc.nextLine();
			if (ControleDonnees.validerMotDePasseClient(mdp) == false) {
				System.out.println("Le mot de passe doit contenir au moins 8 charactére");
				mdp = null;
			}
		} while (mdp == null);

		do {
			System.out.println("Veuillez entrer le nom du conseiller : ");
			nom = sc.next();
			sc.nextLine();
			if (ControleDonnees.validerNomPrenomClient(nom) == false) {
				System.out.println("Le nom doit contenir que des lettres");
				nom = null;
			}
		} while (nom == null);

		do {
			System.out.println("Veuillez entrer le prenom du conseiller : ");
			prenom = sc.next();
			sc.nextLine();

			if (ControleDonnees.validerNomPrenomClient(prenom) == false) {
				System.out.println("Le prenom doit contenir que des lettres");
				prenom = null;
			}
		} while (prenom == null);
		return new Conseiller(nom, prenom, mdp);
	}

	public Client RechercheClient(Conseiller conseiller, String idClient) {
		for (Client client : conseiller.getClients()) {
			if (idClient.equals(client.getId()) && client.isActive() == true)
				return client;
		}
		return null;
	}

	public Compte RechercheCompte(Conseiller conseiller, String numCompte) {
		ClientService cs = new ClientService();
		for (Client client : conseiller.getClients()) {
			return cs.RechercheCompte(client, numCompte);
		}
		return null;
	}

	public Client RechercheClientLogin(Conseiller conseiller, String loginClient) {
		for (Client client : conseiller.getClients()) {
			if (loginClient.equals(client.getLoginClient()) && client.isActive() == true)
				return client;
		}
		return null;
	}

}
