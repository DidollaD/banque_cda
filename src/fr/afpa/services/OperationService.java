package fr.afpa.services;

import fr.afpa.entite.Operation;

/**
 * Operation services : touts les types de operations
 * @author 59013-70-12
 *
 */
public class OperationService {
	public Operation creerOperation(String typeOperation, double montant) {
		return new Operation(typeOperation, montant);
	}
}
