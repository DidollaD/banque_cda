package fr.afpa.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import fr.afpa.control.ControleDonnees;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;

public class ClientService {
	
	/**
	 * Methode pour creer un nouveau client
	 * @param listAgence : agence du nouveau client
	 * @param sc : scanner pour recuperer les reponses
	 * @return
	 */
	public Client creerNouveauClient(ArrayList<Agence> listAgence,Scanner sc) {

		String nom = null;
		String prenom = null;
		LocalDate localDate = null;
		String mail = null;
		String mdp = null;
		String login = GenererLoginAlea(listAgence);
		
		do {
			System.out.println("Veuillez entrer le mdp du client : ");
			mdp = sc.next();
			sc.nextLine();
			if (ControleDonnees.validerMotDePasseClient(mdp) == false) {
				System.out.println("Le mot de passe doit contenir au moins 8 charact�re");
				mdp = null;
			}
		} while (mdp == null);

		do {
			System.out.println("Veuillez entrer le nom du client : ");
			nom = sc.next();
			sc.nextLine();
			if (ControleDonnees.validerNomPrenomClient(nom) == false) {
				System.out.println("Le nom doit contenir que des lettres");
				nom = null;
			}
		} while (nom == null);

		do {
			System.out.println("Veuillez entrer le prenom du client: ");
			prenom = sc.next();
			sc.nextLine();
			if (ControleDonnees.validerNomPrenomClient(prenom) == false) {
				System.out.println("Le prenom doit contenir que des lettres");
				prenom = null;
			}
		} while (prenom == null);

		do {
			System.out.println("Veuillez entrer la date de naissance du client : jj/mm/yyyy");
			String date = sc.next();
			sc.nextLine();
			String[] dateform = date.split("/");
			try {
				localDate = LocalDate.parse(dateform[2] + "-" + dateform[1] + "-" + dateform[0]);
				if (ControleDonnees.ValiderDate(localDate) == false) {
					System.out.println("Veuillez entrer une date valide");
					localDate = null;
				}
			} catch (Exception e) {
				System.out.println("Veuillez entrer une date valide");
				localDate = null;
			}
		} while (localDate == null);

		do {
			System.out.println("Veuillez entrer le mail du client : ");
			mail = sc.next();
			sc.nextLine();
			if (ControleDonnees.validerAdresseMailClient(mail) == false) {
				System.out.println("Veuillez entrer un email valide");
				mail = null;
			}
		} while (mail == null);
		return new Client(login, mdp, nom, prenom, mail, localDate);
	}
	

	/**
	 * Methode pour rechercher un compte client
	 * @param client : client proprietaire du compte
	 * @param numCompte : numero de compte
	 * @return
	 */

	public Compte RechercheCompte(Client client, String numCompte) {
		for (Compte compte : client.getComptes()) {
			if (numCompte.equals(compte.getNumeroCompte()) && compte.isActive() == true) {
				return compte;
			}
		}
		return null;
	}

	/**
	 * Methode pour rechercher un compte actif
	 * @param client : client proprietaire du compte
	 * @return
	 */
	public int rechercherNbrCompteActif(Client client) {
		int i = 0;
		for (Compte compte : client.getComptes()) {
			if (compte.isActive() == true) {
				i++;
			}
		}
		return i;
	}

	/**
	 * Methode pour desactiver un client 
	 * @param listAgence : permets de recuperer l agence du client
	 * @param sc : recuperer la reponse
	 */
	public void desactiverClient(ArrayList<Agence> listAgence, Scanner sc) {
		UtilService us = new UtilService();
		Client client = null;
		do {
			System.out.println("Quel client voulez vous desactiver ?");
			String idClient = sc.next();
			sc.nextLine();
			client = us.RechercheClient(listAgence, idClient);
			if (client == null)
				System.out.println("client introuvable");
		} while (client == null);
		client.setActive(false);
	}

	private String GenererLoginAlea(ArrayList<Agence> listAgence) {
		UtilService us = new UtilService();
		String login = "";
		do {
			for (int i = 0; i < 11; i++) {
				login += new Random().nextInt(9);
			}
			if (us.RechercheClientLogin(listAgence, login) != null) {
				login = null;
			}
		} while (login == null);

		return login;
	}

	/**
	 * Methode pour changer de domiciliation du client  
	 * @param nouvConseiller : le nouveau conseiller du client
	 * @param conseiller : l'ancien conseiller
	 * @param client : le client 
	 * @param sc : recuperer la reponse
	 * @return
	 */

	public boolean changerDomiciliationClient(Conseiller nouvConseiller, Conseiller conseiller, Client client,
			Scanner sc) {
		nouvConseiller.getClients().add(client);
		conseiller.getClients().remove(client);
		return true;
	}

	/**
	 * Methode qui permets de modifier les informations du client
	 * 
	 * @param client : client concerne
	 * @param sc : recuperer la reponse
	 */

	public void modifierInfoClient(Client client, Scanner sc) {
		String nom = null;
		String prenom = null;
		LocalDate localDate = null;
		String email = null;
		do {
			System.out.println(
					" Modification � faire sur le nom? (Tapez entrer pour continuer sans rien entrer pour ne pas modifier)");
			nom = sc.next();
			sc.nextLine();
			if (nom != null) {
				if (ControleDonnees.validerNomPrenomClient(nom) == true) {
					client.setNom(nom);
					nom = null;
				}
			}

		} while (nom != null);

		do {
			System.out.println(
					" Modification � faire sur le prenom? (Tapez entrer pour continuer sans rien entrer pour ne pas modifier)");
			prenom = sc.next();
			sc.nextLine();
			if (prenom != null) {
				if (ControleDonnees.validerNomPrenomClient(prenom) == true) {
					client.setPrenom(prenom);
					prenom = null;
				}
			}

		} while (prenom != null);

		do {
			System.out.println(
					" Modification � faire sur la date de naissance? jj/mm/yyyy (Tapez entrer pour continuer sans rien entrer pour ne pas modifier)");
			String date = sc.next();
			sc.nextLine();
			String[] dateform = date.split("/");
			try {
				localDate = LocalDate.parse(dateform[2] + "-" + dateform[1] + "-" + dateform[0]);
				if (ControleDonnees.ValiderDate(localDate) == true) {
					client.setDateNaissance(localDate);
					localDate = null;
				}
			} catch (Exception e) {
				System.out.println("Veuillez entrer une date valide");
				localDate = LocalDate.of(0, 0, 0);
			}
		} while (localDate != null);

		do {
			System.out.println(
					" Modification � faire sur l'email? (Tapez entrer pour continuer sans rien entrer pour ne pas modifier)");
			email = sc.next();
			sc.nextLine();
			if (email != null) {
				if (ControleDonnees.validerAdresseMailClient(email) == true) {
					client.setEmail(email);
					email = null;
				}
			}
		} while (email != null);

	}
}
