package fr.afpa.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.control.ControleSoldeCompte;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.Operation;
import fr.afpa.entite.PlanEpargneLogement;

public class CompteService {

	/**
	 * Methode qui permets d'alimenter le compte
	 * @param compte : compte concerne
	 * @param montant : montant a verser 
	 * @param sc : lire reponse
	 */
	public void alimenterCompteBancaire(Compte compte, double montant, Scanner sc) {
		compte.setSolde(compte.getSolde() + montant);
		compte.getOperations().add(new Operation("versement", montant));
		System.out.println("Operation reussie");
	}

	/**
	 *  Methode qui permets de faire un retrait sur le compte
	 * @param compte: compte concerne
	 * @param montant:montant a verser 
	 * @param sc : lire reponse
	 */

	public void RetirerArgentCompteBancaire(Compte compte, double montant, Scanner sc) {
		if (compte.isDecouvertAutorise() == false) {
			if (montant > compte.getSolde()) {
				System.out.println("Solde insuffisant");
			} else {
				compte.setSolde(compte.getSolde() - montant);
				compte.getOperations().add(new Operation("retrait", montant));
				System.out.println("Operation reussie");
			}
		} else {
			if (compte.getSolde() < 0) {
				System.out.println("Solde n�gatif");
			} else {
				compte.setSolde(compte.getSolde() - montant);
				compte.getOperations().add(new Operation("retrait", montant));
				System.out.println("Operation reussie");
			}
		}
	}

	/**
	 * Methode qui permet de faire un virement
	 * @param cp1 : compte de depart
	 * @param sc : recuperer la reponse
	 * @param montant : montant du virement
	 * @param agences : agences des comptes concernes
	 */
	public void faireUnVirement(Compte cp1, Scanner sc, double montant, ArrayList<Agence> agences) {
		UtilService us = new UtilService();
		Compte cp2 = null;
		if (ControleSoldeCompte.validerSoldeCompte(montant, cp1)) {
			System.out.println("Entrez le num�ro du compte � cr�diter");
			String numCompte = sc.next();
			sc.nextLine();
			cp2 = us.RechercheCompte(agences, numCompte);
			if (cp2 != null && cp1 != null) {
				cp1.setSolde(cp1.getSolde() - montant);
				cp1.getOperations().add(new Operation("retrait", montant));
				cp2.setSolde(cp2.getSolde() + montant);
				cp2.getOperations().add(new Operation("versement", montant));
				System.out.println("oui");
			}
		}
	}

	/**
	 * M�thode qui permets d'imprimer un compte
	 * 
	 * @param compte : compte concerne
	 * @param dateDebut : date de debut 
	 * @param dateFin : date de fin
	 */
	public void ImprimerCompte(Compte compte, LocalDate dateDebut, LocalDate dateFin) {
		FileWriter writer;
		try {
			writer = new FileWriter(".\\save\\Compte" + compte.getNumeroCompte() + ".txt");
			BufferedWriter document = new BufferedWriter(writer);
			document.write("Compte : " + compte.getNumeroCompte());
			document.newLine();
			for (Operation operation : compte.getOperations()) {
				if ((operation.getDateOperation().isEqual(dateDebut) || operation.getDateOperation().isAfter(dateDebut))
						&& (operation.getDateOperation().isEqual(dateFin)
								|| operation.getDateOperation().isBefore(dateFin))) {
					document.write(operation.toString());
					document.newLine();
				}
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * M�thode pour cloturer un compte
	 * 
	 * @param listAgence : agence a laquelle appartient le compte
	 * @param sc : recuperer la reponse
	 */

	public void cloturerCompte(ArrayList<Agence> listAgence, Scanner sc) {
		UtilService us = new UtilService();
		Compte compte = null;
		do {
			System.out.println("Quel compte voulez vous supprimer ?");
			String idCompte = sc.next();
			sc.nextLine();
			compte = us.RechercheCompte(listAgence, idCompte);
			if (compte == null)
				System.out.println("compte introuvable");
		} while (compte == null);
		compte.setActive(false);
	}

	/**
	 * Methode qui permets de creer un compte
	 * 
	 * @param client nouveau client
	 * @return
	 */

	public Compte creerCompte(Client client, Scanner sc) {
		Compte compte = null;
		String typeCompte = null;
		boolean decouvertAutorise = false;
		char choix = '0';
		ClientService cs = new ClientService();
		if (cs.rechercherNbrCompteActif(client) == 3) {
			System.out.println("Ce Client Possede deja 3 compte actif");
		} else if (cs.rechercherNbrCompteActif(client) == 0) {
			typeCompte = "Compte courant";
		} else {
			System.out.println("vouler-vous cree : ");
			System.out.println("   1-un Compte courant");
			System.out.println("   2-un Livret A");
			System.out.println("   3-un Plan �pargne logement");
			do {
				System.out.println("votre choix : ");
				choix = sc.next().charAt(0);
				sc.nextLine();
				switch (choix) {
				case '1':
					do {
						System.out.println("le compte autorise-t-il le decouvert : ");
						choix = sc.next().toLowerCase().charAt(0);
						sc.nextLine();
						if (choix == 'o') {
							decouvertAutorise = true;
						} else if (choix == 'n') {
							decouvertAutorise = false;
						} else {
							System.out.println("saisie invalide");
						}
					} while (choix != 'o' && choix != 'n');
					compte = new CompteCourant(decouvertAutorise);
					break;
				case '2':
					compte = new LivretA();
					break;
				case '3':
					compte = new PlanEpargneLogement();
					break;
				default:
					System.out.println("saisie invalide");
					break;
				}
			} while (typeCompte == null);
		}

		return compte;
	}
}
