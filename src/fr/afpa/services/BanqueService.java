package fr.afpa.services;

//import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;

public class BanqueService {
	private ArrayList<Agence> listAgences;
	private Admin admin;
	
	/**
	 * Methode qui regroupe les services de la banque
	 */
	public BanqueService() {
		Scanner sc = new Scanner(System.in);
		UtilService us = new UtilService();
		AgenceService as = new AgenceService();
		admin= new Admin();
		listAgences = as.creerListAgence();
		Personne typeUser = us.Authentification(listAgences,admin,sc);
		if (typeUser instanceof Admin) {
			us.choixMenuAdmin(listAgences,sc);
		} else if (typeUser instanceof Conseiller) {
			us.choixMenuConseiller((Conseiller)typeUser,listAgences, sc);
		} else if (typeUser instanceof Client) {
			us.choixMenuClient((Client)typeUser,listAgences,sc);
		}

//		try {
//			us.Sauvegarde(listAgences);
//		} catch (IOException e) {
//			System.out.println("sauvegarde �chou�");
//		}
		sc.close();
	}

}
