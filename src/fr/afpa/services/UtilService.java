package fr.afpa.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.control.ControleDonnees;
import fr.afpa.control.UtilControl;
import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;
import fr.afpa.ihm.Affichage;
import fr.afpa.ihm.ClientAffichage;

public class UtilService {
	
	/**
	 * Methode pour s'authentifier
	 * @param agences : toutes les agences
	 * @param admin : admin du programme
	 * @param sc : recuperer les reponses
	 * @return
	 */

	public Personne Authentification(ArrayList<Agence> agences, Admin admin, Scanner sc) {
		Personne result = null;
		String entrer = null;
		do {
			do {
				System.out.println("Saisir votre login");
				entrer = sc.next();
				sc.nextLine();
				result = UtilControl.ControlLogin(agences, admin, entrer, sc);
			} while (result == null);
			return result;
		} while (true);
	}

	/**
	 * Methode qui permets de rechercher une agence
	 * @param agences : liste des agences
	 * @param codeAgence : le code de une agence
	 * @return
	 */
	public Agence RechercheAgence(ArrayList<Agence> agences, String codeAgence) {
		for (Agence agence : agences) {
			if (codeAgence.equals(agence.getCodeAgence()))
				return agence;
		}
		return null;
	}

	/**
	 * Methode pour rechercher un conseiller
	 * @param agences : liste des agences
	 * @param loginConseiller : conseiller concerne
	 * @return
	 */
	public Conseiller RechercheConseiller(ArrayList<Agence> agences, String loginConseiller) {
		AgenceService as = new AgenceService();
		for (Agence agence : agences) {
			return as.RechercheConseiller(agence, loginConseiller);
		}
		return null;
	}

	/**
	 * Methode pour rechercher un compte
	 * @param agences : liste des agences
	 * @param numCompte : numero de compte
	 * @return
	 */
	public Compte RechercheCompte(ArrayList<Agence> agences, String numCompte) {
		AgenceService as = new AgenceService();
		for (Agence agence : agences) {
			return as.RechercheCompte(agence, numCompte);
		}
		return null;
	}
	/**
	 * Methode pour rechercher un client avec son login
	 * @param agences : liste des agences
	 * @param loginClient : login client
	 * @return
	 */

	public Client RechercheClientLogin(ArrayList<Agence> agences, String loginClient) {
		AgenceService as = new AgenceService();
		for (Agence agence : agences) {
			return as.RechercheClientLogin(agence, loginClient);
		}
		return null;
	}

	/**
	 * Methode qui permets de rechercher le client a partir de son id
	 * @param agences : liste des agences
	 * @param idClient: identifiant du client
	 * @return
	 */
	public Client RechercheClient(ArrayList<Agence> agences, String idClient) {
		AgenceService as = new AgenceService();
		for (Agence agence : agences) {
			return as.RechercheClient(agence, idClient);
		}
		return null;
	}
//	public void Sauvegarde(ArrayList<Agence> listAgences) throws IOException {
//		FileWriter writer = new FileWriter(".\\save\\ListeAgence.csv");
//		BufferedWriter document = new BufferedWriter(writer);
//		document.write("codeAgence;numRue;rue;cp;ville");
//		document.newLine();
//		for (Agence agence : listAgences) {
//			document.write(agence.getCodeAgence() + ";" + agence.getNumAddrAgence() + ";" + agence.getRueAgence() + ";"
//					+ agence.getCodePostalAgence() + ";" + agence.getVilleAgence() + ";");
//			for (Conseiller conseiller : agence.getConseillers()) {
//				document.write(conseiller.getIdConseiller() + "\\|" + conseiller.getMdpConseiller() + "\\|"
//						+ conseiller.getNomConseiller() + "\\|" + conseiller.getPrenomConseiller() + ";");
//				for (Client client : conseiller.getClients()) {
//					document.write(client.getIdClient() + "{" + client.getLoginClient() + "{" + client.getMdpClient()
//							+ "{" + client.getNom() + "{" + client.getPrenom() + "{" + client.getEmail() + "{"+ client.isActive() + "\\|");
//					for (Compte compte  : client.getComptes()) {
//						document.write(compte.getNumeroCompte()+ "[" +compte.getTypeCompte()+ "[" +compte.getSolde()+ "[" +compte.isActive()+ "{");
//						for (Operation operation : compte.getOperations()) {
//							document.write(operation.getTypeOperation()+ "~" +operation.getSomme()+ "~" +operation.getCompteCredite()+ "[");
//						}
//					}
//				}
//			}
//			document.newLine();
//		}
//		document.close();
//	}

	/**
	 * Methode qui permets de faire le choix du menu client
	 * @param client : client concerne
	 * @param agences : liste des agences
	 * @param sc : scanner pour recuperer les reponses
	 */
	public void choixMenuClient(Client client, ArrayList<Agence> agences, Scanner sc) {
		char choix = '0';
		double montant = 0;
		Compte compte = null;
		Affichage a = new Affichage();
		ClientAffichage ca = new ClientAffichage();
		CompteService cps = new CompteService();
		do {
			a.menuClient();
			System.out.println("Q - Quitter\n\nVotre choix : ");
			choix = sc.next().toLowerCase().charAt(0);
			sc.nextLine();
			switch (choix) {
			case 'a':
				ca.ConsulterInformation(client);
				break;
			case 'b':
				ca.ConsulterCompte(client);
				break;
			case 'c':
				ca.ConsulterOperationCompte(client);
				break;
			case 'd':
				compte = null;
				montant = 0;
				do {
					System.out.println("Entrez le compte � d�biter");
					compte = SaisirNumCompte(client, sc);
					if (compte == null)
						System.out.println("Client introuvable");
				} while (compte == null);
				do {
					System.out.println("Entrez le montant � transferer");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.faireUnVirement(compte, sc, montant, agences);
				break;
			case 'e':
				LocalDate dateDebut = null;
				LocalDate dateFin = null;
				compte = null;
				do {
					System.out.println("Entrer id du compte");
					compte = SaisirNumCompte(client, sc);
					if (compte == null)
						System.out.println("Client introuvable");

				} while (compte == null);
				do {
					System.out.println("Veuillez entrer la date de debut : jj/mm/yyyy");
					dateDebut = SaisirDate(sc);
				} while (dateDebut == null);
				do {
					System.out.println("Veuillez entrer la date de fin : jj/mm/yyyy");
					dateFin = SaisirDate(sc);
					if (ControleDonnees.ValiderDateFin(dateDebut, dateFin) == false) {
						System.out.println("Veuillez entrer une date valide");
						dateFin = null;
					}
				} while (dateFin == null);
				cps.ImprimerCompte(compte, dateDebut, dateFin);
				break;
			case 'f':
				do {
					System.out.println("Quel compte voulez vous crediter ?");
					compte = SaisirNumCompte(client, sc);
					if (compte == null)
						System.out.println("Compte introuvable");
				} while (compte == null);
				do {
					System.out.println("Entrer la somme");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.alimenterCompteBancaire(compte, montant, sc);

				break;
			case 'g':
				do {
					System.out.println("Quel compte voulez vous debiter ?");
					compte = SaisirNumCompte(client, sc);
					if (compte == null)
						System.out.println("Compte introuvable");
				} while (compte == null);
				do {
					System.out.println("Entrer la somme");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.RetirerArgentCompteBancaire(compte, montant, sc);
				break;
			case 'q':
				break;
			default:
				System.out.println("Saisie invalide");
				break;
			}
		} while (choix != 'q');
	}

	/**
	 * Methode qui permets de faire le choix du menu conseiller
	 * @param conseiller : conseiller concerne
	 * @param listAgences : liste des agences
	 * @param sc : scanner
	 */
	public void choixMenuConseiller(Conseiller conseiller, ArrayList<Agence> listAgences, Scanner sc) {
		char choix = '0';
		double montant = 0;
		Affichage a = new Affichage();
		ClientAffichage ca = new ClientAffichage();
		ClientService cs = new ClientService();
		CompteService cps = new CompteService();
		Compte compte = null;
		Client client = null;

		do {
			a.menuConseiller();
			System.out.println("Q - Quitter\n\nVotre choix : ");
			choix = sc.next().toLowerCase().charAt(0);
			sc.nextLine();
			switch (choix) {
			case 'a':
				do {
					client = null;
					System.out.println("De quel client voulez vous voir les Information");
					client = SaisirClient(conseiller, sc);
					if (client == null)
						System.out.println("Client Introuvable");
				} while (client == null);
				ca.ConsulterInformation(client);
				break;
			case 'b':
				client = null;
				do {
					System.out.println("De quel client voulez vous voir les comptes");
					client = SaisirClient(conseiller, sc);
					if (client == null)
						System.out.println("Client Introuvable");
				} while (client == null);
				ca.ConsulterCompte(client);
				break;
			case 'c':
				client = null;
				do {
					System.out.println("De quel client voulez vous voir les operations");
					client = SaisirClient(conseiller, sc);
					if (client == null)
						System.out.println("Saisie invalide");
				} while (client == null);
				ca.ConsulterOperationCompte(client);
				break;
			case 'd':
				compte = null;
				montant = 0;
				do {
					System.out.println("Entrez le compte � d�biter");
					compte = SaisirNumCompte(conseiller, sc);
					if (compte == null)
						System.out.println("compte introuvable");
				} while (compte == null);
				do {
					System.out.println("Entrez le montant � transferer");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.faireUnVirement(compte, sc, montant, listAgences);
				break;
			case 'e':
				LocalDate dateDebut = null;
				LocalDate dateFin = null;
				compte = null;
				do {
					System.out.println("Entrer id du compte");
					compte = SaisirNumCompte(conseiller, sc);
					if (compte == null)
						System.out.println("compte introuvable");

				} while (compte == null);
				do {
					System.out.println("Veuillez entrer la date de debut : jj/mm/yyyy");
					dateDebut = SaisirDate(sc);
				} while (dateDebut == null);
				do {
					System.out.println("Veuillez entrer la date de fin : jj/mm/yyyy");
					dateFin = SaisirDate(sc);
					if (ControleDonnees.ValiderDateFin(dateDebut, dateFin) == false) {
						System.out.println("Veuillez entrer une date valide");
						dateFin = null;
					}
				} while (dateFin == null);
				cps.ImprimerCompte(compte, dateDebut, dateFin);
				break;
			case 'f':
				compte = null;
				do {
					System.out.println("De quel compte voulais-vous crediter");
					compte = SaisirNumCompte(conseiller, sc);
					if (compte == null)
						System.out.println("Saisie invalide");
				} while (compte == null);
				do {
					System.out.println("Entrer la somme");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.alimenterCompteBancaire(compte, montant, sc);
				break;
			case 'g':
				compte = null;
				do {
					System.out.println("De quel client voulais-vous d�biter");
					compte = SaisirNumCompte(conseiller, sc);
					if (compte == null)
						System.out.println("Saisie invalide");
				} while (compte == null);
				do {
					System.out.println("Entrer la somme");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.RetirerArgentCompteBancaire(compte, montant, sc);
				break;
			case 'h':
				client = null;
				do {
					System.out.println("Pour quel clients voulez-vous creer un compte ?");
					client = SaisirClient(conseiller, sc);
					if (client == null)
						System.out.println("Saisie invalide");
				} while (client == null);
				client.getComptes().add(cps.creerCompte(client, sc));
				break;
			case 'i':
				conseiller.getClients().add(cs.creerNouveauClient(listAgences, sc));
				break;
			case 'j':
				client = null;
				Agence agence = null;
				Conseiller nouvConseiller = null;
				do {
					System.out.println("Lequel de vos clients voulez vous transferer ?");
					client = SaisirClient(conseiller, sc);
					if (client == null)
						System.out.println("Saisie invalide");
				} while (client == null);

				do {
					System.out.println("Dans quelle agence voulez vous l'assigner ?");
					agence = SaisirAgence(listAgences, sc);
					if (client == null)
						System.out.println("Saisie invalide");
				} while (agence == null);

				do {
					System.out.println("A quel conseiller assignez vous le client ?");
					nouvConseiller = SaisirConseiller(agence, sc);
					if (client == null)
						System.out.println("Saisie invalide");
				} while (nouvConseiller == null);
				cs.changerDomiciliationClient(nouvConseiller, conseiller, client, sc);
				break;
			case 'k':
				break;
			case 'q':
				System.out.println("Au Revoir");
				break;
			default:
				System.out.println("Saisie invalide");
				break;
			}
		} while (choix != 'q');
	}

	/**
	 * Methode qui permets de choisir le menu admin
	 * @param listAgences : liste des agences
	 * @param sc : scanner
	 */
	
	
	public void choixMenuAdmin(ArrayList<Agence> listAgences, Scanner sc) {
		char choix = '0';
		double montant = 0;
		Affichage a = new Affichage();
		ClientAffichage ca = new ClientAffichage();
		CompteService cps = new CompteService();
		ConseillerService cns = new ConseillerService();
		ClientService cs = new ClientService();
		AgenceService as = new AgenceService();
		Compte compte = null;
		Client client = null;
		Conseiller conseiller = null;
		Agence agence = null;

		do {
			a.menuAdmin();
			System.out.println("Q - Quitter\n\nVotre choix : ");
			choix = sc.next().toLowerCase().charAt(0);
			sc.nextLine();
			switch (choix) {
			case 'a':
				client = null;
				do {
					System.out.println("De quel client voulez vous voir les informations");
					client = SaisirClient(listAgences, sc);
					if(client == null)System.out.println("client introuvable");
				} while (client == null);
				ca.ConsulterInformation(client);
				break;
			case 'b':
				client = null;
				do {
					System.out.println("De quel client voulez vous voir les comptes");
					client = SaisirClient(listAgences, sc);
					if(client == null)System.out.println("compte introuvable");
				} while (client == null);
				ca.ConsulterCompte(client);
				break;
			case 'c':
				client = null;
				do {
					System.out.println("De quel client voulez vous voir les comptes");
					client = SaisirClient(listAgences, sc);
					if(client == null)System.out.println("compte introuvable");
				} while (client == null);
				ca.ConsulterOperationCompte(client);
				break;
			case 'd':
				compte = null;
				montant = 0;
				do {
					System.out.println("Entrez le compte � d�biter");
					compte = SaisirNumCompte(listAgences, sc);
					if (compte == null)
						System.out.println("Compte introuvable");
				} while (compte == null);
				do {
					System.out.println("Entrez le montant � transferer");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.faireUnVirement(compte, sc, montant, listAgences);
				break;
			case 'e':
				LocalDate dateDebut = null;
				LocalDate dateFin = null;
				compte = null;
				do {
					System.out.println("Entrer id du compte");
					compte = SaisirNumCompte(listAgences, sc);
					if (compte == null)
						System.out.println("Client introuvable");

				} while (compte == null);
				do {
					System.out.println("Veuillez entrer la date de debut : jj/mm/yyyy");
					dateDebut = SaisirDate(sc);
				} while (dateDebut == null);
				do {
					System.out.println("Veuillez entrer la date de fin : jj/mm/yyyy");
					dateFin = SaisirDate(sc);
					if (ControleDonnees.ValiderDateFin(dateDebut, dateFin) == false) {
						System.out.println("Veuillez entrer une date valide");
						dateFin = null;
					}
				} while (dateFin == null);
				cps.ImprimerCompte(compte, dateDebut, dateFin);
				break;
			case 'f':
				compte = null;
				do {
					System.out.println("De quel compte voulez vous crediter");
					compte = SaisirNumCompte(listAgences, sc);
					if (compte == null)
						System.out.println("compte introuvable");
				} while (compte == null);
				do {
					System.out.println("entrer la somme");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.alimenterCompteBancaire(compte, montant, sc);
				break;
			case 'g':
				compte = null;
				do {
					System.out.println("De quel compte voulez vous crediter");
					compte = SaisirNumCompte(listAgences, sc);
					if (compte == null)
						System.out.println("Saisie invalide");
				} while (compte == null);
				do {
					System.out.println("entrer la somme");
					montant = SaisirMotant(sc);
				} while (montant == 0);
				cps.RetirerArgentCompteBancaire(compte, montant, sc);
				break;
			case 'h':
				client = null;
				do {
					System.out.println("Pour quel clients voulez-vous creer un compte ?");
					client = SaisirClient(listAgences, sc);
					if (client == null)
						System.out.println("client introuvable");
				} while (client == null);
				client.getComptes().add(cps.creerCompte(client, sc));
				break;
			case 'i':
				conseiller = null;
				do {
					System.out.println("A quel conseiller voulez-vous assigner le nouveau client");
					conseiller = SaisirConseiller(listAgences, sc);
					if (conseiller == null)
						System.out.println("conseiller introuvable");
				} while (conseiller == null);
				conseiller.getClients().add(cs.creerNouveauClient(listAgences, sc));
				break;
			case 'j':
				client = null;
				agence = null;
				Conseiller nouvConseiller = null;
				do {
					System.out.println("Lequel de vos clients voulez vous transferer ?");
					client = SaisirClient(listAgences, sc);
					if (client == null)
						System.out.println("client introuvable");
				} while (client == null);

				do {
					System.out.println("Dans quelle agence voulez vous l'assigner ?");
					agence = SaisirAgence(listAgences, sc);
					if (client == null)
						System.out.println("agence introuvable");
				} while (agence == null);

				do {
					System.out.println("A quel conseiller assignez vous le client ?");
					nouvConseiller = SaisirConseiller(agence, sc);
					if (client == null)
						System.out.println("conseiller introuvable");
				} while (nouvConseiller == null);
				cs.changerDomiciliationClient(nouvConseiller, conseiller, client, sc);
				break;
			case 'k':
				break;
			case 'l':
				listAgences.add(as.creerAgence(sc));
				break;
			case 'm':
				agence = null;
				do {
					System.out.println("Dans quelle agence se trouve le conseiller");
					agence = SaisirAgence(listAgences, sc);
					if (client == null)
						System.out.println("agence introuvable");
				} while (agence == null);
				agence.getConseillers().add(cns.creerNouveauConseiller(sc));
				break;
			case 'n':
				cps.cloturerCompte(listAgences, sc);
				break;
			case 'o':
				cs.desactiverClient(listAgences, sc);
				break;
			case 't' :
				 a.AfficherTout(listAgences);
				 break;
			case 'q':
				System.out.println("Au Revoir");
				break;
			default:
				System.out.println("Saisie invalide");
				break;
			}
		} while (choix != 'q');
	}

	/**
	 * Methode pour chercher le compte dans la liste de comptes d un client 
	 * @param client : client concerne
	 * @param sc : scanner
	 * @return
	 */
	private Compte SaisirNumCompte(Client client, Scanner sc) {
		ClientService cs = new ClientService();
		String numCompte = sc.next();
		sc.nextLine();
		return cs.RechercheCompte(client, numCompte);
	}
	/**
	 * Methode pour chercher le compte dans la liste de comptes d un client et d un conseiller
	 * @param conseiller
	 * @param sc
	 * @return
	 */

	private Compte SaisirNumCompte(Conseiller conseiller, Scanner sc) {
		ConseillerService cns = new ConseillerService();
		String numCompte = sc.next();
		sc.nextLine();
		return cns.RechercheCompte(conseiller, numCompte);
	}

	/**
	 * Methode pour chercher le compte dans la liste d agences 
	 * @param listAgences
	 * @param sc
	 * @return
	 */
	private Compte SaisirNumCompte(ArrayList<Agence> listAgences, Scanner sc) {
		String numCompte = sc.next();
		sc.nextLine();
		return RechercheCompte(listAgences, numCompte);
	}

	/**
	 * Methode qui permets de saisir la date 
	 * @param sc : recuperer la reponse
	 * @return
	 */
	private LocalDate SaisirDate(Scanner sc) {
		LocalDate localDate = null;
		String date = sc.next();
		sc.nextLine();
		String[] dateform = date.split("/");
		try {
			localDate = LocalDate.parse(dateform[2] + "-" + dateform[1] + "-" + dateform[0]);
			if (ControleDonnees.ValiderDate(localDate) == false) {
				System.out.println("Veuillez entrer une date valide 1");
				localDate = null;
			}
		} catch (Exception e) {
			System.out.println("Veuillez entrer une date valide 2");
			localDate = null;
		}
		return localDate;
	}

	/**
	 * Methode qui permets de saisir le montant
	 * @param sc : recupere la reponse
	 * @return
	 */
	private double SaisirMotant(Scanner sc) {
		double montant = 0;
		try {
			montant = sc.nextDouble();
			sc.nextLine();
		} catch (Exception e) {
			System.out.println("Saisie invalide");
			montant = 0;
		}
		if (UtilControl.verifierMontant(montant) == false) {
			System.out.println("Saisie invalide");
			montant = 0;
		}
		return montant;
	}
/**
 * Methode quiprmets de saisir un client
 * @param conseiller
 * @param sc
 * @return
 */
	private Client SaisirClient(Conseiller conseiller, Scanner sc) {
		ConseillerService cns = new ConseillerService();
		String idClient = sc.next();
		sc.nextLine();
		return cns.RechercheClient(conseiller, idClient);
	}
	/**
	 * Methode qui permets de saisir un client
	 * @param listAgences
	 * @param sc
	 * @return
	 */

	private Client SaisirClient(ArrayList<Agence> listAgences, Scanner sc) {
		String idClient = sc.next();
		sc.nextLine();
		return RechercheClient(listAgences, idClient);
	}
	/**
	 * Methode qui permets de saisir une agence
	 * @param listAgences
	 * @param sc
	 * @return
	 */

	private Agence SaisirAgence(ArrayList<Agence> listAgences, Scanner sc) {
		String idAgence = sc.next();
		sc.nextLine();
		return RechercheAgence(listAgences, idAgence);
	}
/**
 * Methode qui permets de saisir un conseiller
 * @param agence
 * @param sc
 * @return
 */
	private Conseiller SaisirConseiller(Agence agence, Scanner sc) {
		AgenceService as = new AgenceService();
		String idConseiller = sc.next();
		sc.nextLine();
		return as.RechercheConseiller(agence, idConseiller);
	}

	/**
	 * Methode pour saisir un conseiller
	 * @param listAgences
	 * @param sc
	 * @return
	 */
	private Conseiller SaisirConseiller(ArrayList<Agence> listAgences, Scanner sc) {
		String idConseiller = sc.next();
		sc.nextLine();
		return RechercheConseiller(listAgences, idConseiller);
	}
}
